#!/bin/python
'''
Salva tutti i documenti presenti nell'archivio del comune di Latina, in cartelle separate.
'''

import os, threading, requests,string
from sys import stderr
import requests
from bs4 import BeautifulSoup


SEZIONI = {
        "Delibere Giunta"   : "http://trasparenza.comune.latina.it/web/albo/delibere-di-giunta?p_auth=HPbDh3vX&p_p_id=jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet_action=eseguiPaginazione",
        "Delibere Consiglio": "http://trasparenza.comune.latina.it/web/albo/delibere-di-consiglio?p_auth=HPbDh3vX&p_p_id=jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet_action=eseguiPaginazione",
        "Determinazioni"    : "http://trasparenza.comune.latina.it/web/albo/determinazioni?p_auth=HPbDh3vX&p_p_id=jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet_action=eseguiPaginazione",
        "Ordinanze"         : "http://trasparenza.comune.latina.it/web/albo/ordinanze?p_auth=HPbDh3vX&p_p_id=jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet_action=eseguiPaginazione",
        "Avvisi Gara"       : "http://trasparenza.comune.latina.it/web/albo/avvisi-di-gara?p_auth=HPbDh3vX&p_p_id=jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet_action=eseguiPaginazione",
        "Altri Atti"        : "http://trasparenza.comune.latina.it/web/albo/altri-atti?p_auth=HPbDh3vX&p_p_id=jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_jcitygovalbopubblicazioni_WAR_jcitygovalbiportlet_action=eseguiPaginazione"
}

VALID_CHARS = "-_.() %s%s" % (string.ascii_letters, string.digits)
urls = []
MAX_THREADS = 5
n_riga = 1

def get_full_page(url):
    ''' Fa l'hack di ottenere tutti gli atti in una sola richiesta grazie
        a un parametro accettato nelle richieste POST
    '''
    res = requests.post(url, data={'hidden_page_size':'20000'})
    return res

def save(url, filename):
    ''' Salva il file all'url indicato nel file del percorso indicato. 
        Sovrascrive sempre il file. '''
    print('salvo %s:\n\t' % filename)
    try:
        res = requests.get(url)
    except requests.exceptions.InvalidSchema:
        print("WARNING: impossibile ottenere", url, file=stderr)
        return
    with open(filename,'wb') as f:
        f.write(res.content)


def download_all(url_filename_list):
    ''' Prende in input una lista di coppie del tipo (url, filename)
    e scarica tutto cio' che c'e' nella lista, aggiungendo un prefisso
    contatore.
    Lo fa con i thread per permettere molte richieste contemporaneamente.
    Il numero massimo di thread e' MAX_THREADS '''
    c = 1 # contatore da prefiggere ai filename
    threads = [] # lista dei threads attivi
    for url, filename in url_filename_list:
        path = "{}_{}.pdf".format(filename[:90],c)
        # evitiamo di riscaricare se gia' ci sta
        if os.path.isfile(path):
            print("%s esiste gia'" % path, file=stderr)
            return
        # avvio del thread separato
        t = DownloaderWorker(url, path)
        threads.append(t) 
        t.start()
        if len(threads) > MAX_THREADS: 
            t = threads.pop(0)
            t.join()
        c+=1
    # prima di uscire, attendiamo la terminazione di tutti i threads
    if len(threads) > 0:
        for t in threads: t.join()

        

def replace_all(string, before, after):
    ''' Sostituisce tutte le occorrenze di before con after 
    all'interno della stringa "string" '''
    while string.find(before) >= 0:
        string = string.replace(before, after)
    return string

class ScrapingWorker(threading.Thread):
    '''
    Thread che si occupa di:
        1. Ricevere un elemento della lista di tutti gli atti
        2. Estrarre e scaricare il link della pagina dei dettagli dell'atto
        3. Estrarre il link del pdf dalla pagina dei dettagli
        4. Aggiungere alla lista globale "urls" una coppia (url, filename),
               dove url e' l'url del pdf, e filename e' il nome del file.
    '''
    def __init__(self, row, riga):
        super().__init__()
        self.row = row # oggetto che rappresenta una riga della tabella HTML
        self.n_riga = riga
        
    def is_da_scaricare(self, title):
        '''Dice se il documento da scaricare e' un pdf visto
        che delle firme digitali non ce ne frega un cazzo'''
        return title != 'Download versione firmata'
        
    def run(self):
        # variabili globali nei thread perche' ogni riga di python e' atomica bitch
        global urls, n_riga
        #print('{}: faccio una riga'.format(self.n_riga))
        # troviamo il link al pdf
        a = self.row.find('a',title='Visualizza Allegato')
        if a is None: return
        a = a['href']
        # Qui si deve scaricare ogni volta un'altra pagina
        try:
            response = requests.get(a)
        except requests.exceptions.ConnectionError:
            # riprova senza ritegno
            response = requests.get(a)
        print(a, 'STATUS CODE',response.status_code)
        # scrapiamo
        soup = BeautifulSoup(response.text, 'lxml') # getto e zuppifico
        # trova l'elemento tabella degli allegati
        table = soup.find('table', class_='allegati-table')
        #if not table: return
        # filtra solo gli allegati che ci interessano
        links = table.find_all('a',
                    class_='btn-small', 
                    title=self.is_da_scaricare)
        # trova il filename
        name = self.row.find('td',class_='oggetto').text[:100]
        name = replace_all(name,'/',' ')
        # inserisci url e filename nella lista
        for a in links:
            urls.append( (a['href'], '{}_{}'.format(self.n_riga, name) ) )


class DownloaderWorker(threading.Thread):
    '''
    Thread che si occupa di SCARICARE UN SINGOLO FILE, dati
    in input l'url a cui trovarlo, e il filename con cui salvarlo,
    piu' il contatore "c" che mette un numero alla fine del filename
    '''
    def __init__(self, url, filename):
        super().__init__()
        self.url = url
        self.filename = ''.join((c for c in filename if c in VALID_CHARS))

    def run(self):
        save(self.url,self.filename)

def scrapa(soup):
    '''Preso in input l'oggetto BeautifulSoup della lista
    di atti del comune, ha il fine di scaricarli tutti;
    per farlo, prima crea (con diversi thread) una lista 
    "urls" di coppie (url,filename) degli oggetti da scaricare,
    per poi darla in pasto a download_all.'''
    global urls, n_riga
    urls= []
    n_riga = 1
    threads = []
    print('cerco la tabella')
    l = soup.find("table")
    print('trovata la tabella')
    rows = l.find_all('tr')[1:]
    print( "\tci sono {} elementi nella tabella".format(len(rows)))
    for row in rows:
        a = row.find('a',title='Visualizza Allegato')
        if a is None: continue
        a = a['href']
        # si deve creare un thread per ogni riga della 
        # tabella che bisogna scrapare
        t = ScrapingWorker(row, n_riga)
        threads.append(t)
        t.start()
        if len(threads) > MAX_THREADS: 
            t = threads.pop(0)
            t.join()
        n_riga += 1
    if len(threads) > 0:
        for t in threads: t.join()
    download_all(urls)

def main():
    if not os.path.exists('data'):
        os.mkdir('data')
    os.chdir('data')
    for nome in SEZIONI:
        #if not nome in 'Determinazioni': continue
        url = SEZIONI[nome]
        print('inizio a scrapare',nome)
        res = get_full_page(url)
        print("\tinzuppo")
        zuppa = BeautifulSoup(res.text,'lxml')
        print('\tinzuppato')

        #print(len( zuppa.find('table').find_all('tr') ) ); continue

        nome_cartella = nome
        if not os.path.exists(nome_cartella):
            os.mkdir(nome_cartella)
        print("entro in {}".format(nome_cartella))
        os.chdir(nome_cartella)
        print("entrato in {}".format(nome_cartella))
        scrapa(zuppa)
        os.chdir('..')


def main_with_files():
    '''DELET THIS'''
    for filename in os.listdir():
        if not filename.endswith('html'): continue
        print('APRO: {}'.format(filename), file=stderr)
        f = open(filename)
        print("inzuppo")
        zuppa = BeautifulSoup(f.read(), 'lxml')
        print('inzuppato')
        nome_cartella = filename[ : filename.rfind('.') ]
        if not os.path.exists(nome_cartella):
            os.mkdir(nome_cartella)
        print("entro in {}".format(nome_cartella))
        os.chdir(nome_cartella)
        print("entrato in {}".format(nome_cartella))
        scrapa(zuppa)
        os.chdir('..')


if __name__ == '__main__':
    main()

